/*
 * at24c02.h
 *
 *  Created on: 2022��8��7��
 *      Author: Administrator
 */

#ifndef INC_AT24CXX_H_
#define INC_AT24CXX_H_


#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

#define AT24C02_ADDER	0xA0
#define CHECK_ADDER		0xFF

uint8_t at24cxx_read_single_reg(uint16_t DevAddress,uint8_t reg);
void at24cxx_write_single_reg(uint16_t DevAddress,uint8_t reg, uint8_t data);
void at24cxx_read(uint16_t DevAddress,uint8_t reg,uint8_t *pbuffer,uint16_t numtoread);
void at24cxx_write(uint16_t DevAddress,uint8_t reg,uint8_t *pbuffer,uint16_t numtowrite);
uint8_t at24cxx_check(void);
void at24cxx_rw_test(void);
#ifdef __cplusplus
}
#endif




#endif /* INC_AT24CXX_H_ */
