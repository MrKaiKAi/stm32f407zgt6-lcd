/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usart.h"
#include "lcd_u.h"
#include "at24cxx.h"
#include "touch_u.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId led_start_testHandle;
osThreadId key_scanHandle;
osMessageQId Queue_keyHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
void Load_Drow_Dialog(void)
{
	LCD_Clear(WHITE);//清屏
 	POINT_COLOR=BLUE;//设置字体为蓝色
	LCD_ShowString(lcddev.width-24,0,200,16,16,"RST");//显示清屏区域
  	POINT_COLOR=RED;//设置画笔蓝色
}
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void led_start(void const * argument);
void key_scan_task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of Queue_key */
  osMessageQDef(Queue_key, 1, uint8_t);
  Queue_keyHandle = osMessageCreate(osMessageQ(Queue_key), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of led_start_test */
  osThreadDef(led_start_test, led_start, osPriorityBelowNormal, 0, 128);
  led_start_testHandle = osThreadCreate(osThread(led_start_test), NULL);

  /* definition and creation of key_scan */
  osThreadDef(key_scan, key_scan_task, osPriorityIdle, 0, 128);
  key_scanHandle = osThreadCreate(osThread(key_scan), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
	log("STM32F407 STAR !\r\n");
	log(" LCD ID:%x\r\n", lcddev.id); //打印LCD ID
	uint8_t x=0;
	uint8_t lcd_id[12];				//存放LCD ID字符串
	POINT_COLOR = RED;     			//画笔颜色：红色
	sprintf((char*) lcd_id, "LCD ID:%04X", lcddev.id);     //将LCD ID打印到lcd_id数组。

	TP_Init();
	log("[touch] xfac:%d, yfac:%d\r\n", tp_dev.xfac*100000000,tp_dev.yfac*100000000);
	for (;;) {
		switch (x) {
		case 0:
			LCD_Clear(WHITE);
			break;
		case 1:
			LCD_Clear(BLACK);
			break;
		case 2:
			LCD_Clear(BLUE);
			break;
		case 3:
			LCD_Clear(RED);
			break;
		case 4:
			LCD_Clear(MAGENTA);
			break;
		case 5:
			LCD_Clear(GREEN);
			break;
		case 6:
			LCD_Clear(CYAN);
			break;
		case 7:
			LCD_Clear(YELLOW);
			break;
		case 8:
			LCD_Clear(BRRED);
			break;
		case 9:
			LCD_Clear(GRAY);
			break;
		case 10:
			LCD_Clear(LGRAY);
			break;
		case 11:
			LCD_Clear(BROWN);
			break;
		}
		POINT_COLOR = RED;
		LCD_ShowString(30, 40, 210, 24, 24, "Explorer STM32F4");
		LCD_ShowString(30, 70, 200, 16, 16, "TFTLCD TEST");
		LCD_ShowString(30, 90, 200, 16, 16, "ATOM@ALIENTEK");
		LCD_ShowString(30, 110, 200, 16, 16, lcd_id); //显示LCD ID
		LCD_ShowString(30, 130, 200, 12, 12, "2017/4/8");

		if(x++ > 11){
//			x = 0;
			LCD_Clear(WHITE);
			while(1){
				osDelay(10);
				tp_dev.scan(0);
//				log("sta:0x%02x, x:%d , y:%d \r\n",tp_dev.sta,tp_dev.x[0],tp_dev.y[0]);
				if(tp_dev.sta&TP_PRES_DOWN)			//触摸屏被按下
				{
				 	if(tp_dev.x[0]<lcddev.width&&tp_dev.y[0]<lcddev.height)
					{
						if(tp_dev.x[0]>(lcddev.width-24)&&tp_dev.y[0]<16)Load_Drow_Dialog();//清除
						else TP_Draw_Big_Point(tp_dev.x[0],tp_dev.y[0],RED);		//画图
					}
				}
			}
		}
		log("LCD RUNNING \r\n");
		osDelay(1000);
	}
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_led_start */
/**
* @brief Function implementing the led_start_test thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_led_start */
void led_start(void const * argument)
{
  /* USER CODE BEGIN led_start */
	static uint8_t count = 0;
	osEvent event;
//	at24cxx_rw_test();
	/* Infinite loop */
	for (;;)
	{
//		log("[--%s--%d--] running...\r\n",__func__,count++);
		event = osMessageGet(Queue_keyHandle,100);
		if(event.status == osEventMessage)
		{
			log("[--%s--%d--]key : 0x%02x \r\n",__func__,count++,event.value.v);

			if(event.value.v==KEY2_E)	//KEY2按下,则执行校准程序
			{
				LCD_Clear(WHITE);	//清屏
			    TP_Adjust();  		//屏幕校准
				TP_Save_Adjdata();
				Load_Drow_Dialog();
			}
		}
	}
  /* USER CODE END led_start */
}

/* USER CODE BEGIN Header_key_scan_task */
/**
* @brief Function implementing the key_scan thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_key_scan_task */
void key_scan_task(void const * argument)
{
  /* USER CODE BEGIN key_scan_task */
	static uint8_t key_status = 0,key_mark = 0;
	uint16_t count = 0;
  /* Infinite loop */
  for(;;)
	{
		if (HAL_GPIO_ReadPin(KEY2_GPIO_Port, KEY2_Pin) == 1)
		{
			key_status |= 0x88;	//key2
		}
		else{
			key_status &= ~0x08;
		}

		if (HAL_GPIO_ReadPin(KEY3_GPIO_Port, KEY3_Pin) == 0)
			key_status |= 0x44;	//key3
		else
			key_status &= ~0x04;

		if ((key_status & 0x0c) != 0)
		{
			key_mark  = 1;
			if (count++ == 0)
			{
				osMessagePut(Queue_keyHandle, key_status, 1000);		//按下首次发一个
			}
		}
		else if (((key_status & 0x0c) == 0) && key_mark)
		{
			if (count >= 300)
			{														//大于3s长按
				key_status |= 0x30;
			}
			osMessagePut(Queue_keyHandle, key_status, 1000);				//发送按键状态
			count = 0, key_status &= 0,key_mark = 0;
		}
		else
		{
			count = 0, key_status &= 0,key_mark = 0;
		}

		osDelay(10);
	}
  /* USER CODE END key_scan_task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */
