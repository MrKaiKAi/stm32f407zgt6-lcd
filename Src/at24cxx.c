/*
 * at24c02.c
 *
 *  Created on: 2022年8月7日
 *      Author: Administrator
 */
#include "at24cxx.h"


//在AT24CXX指定地址读出一个数据
//ReadAddr:开始读数的地址
//返回值  :读到的数据
uint8_t at24cxx_read_single_reg(uint16_t DevAddress,uint8_t reg)
{
    uint8_t res = 0;
    HAL_I2C_Mem_Read(&hi2c1, DevAddress, reg,I2C_MEMADD_SIZE_8BIT,&res,1,10);
    return res;
}

void at24cxx_write_single_reg(uint16_t DevAddress,uint8_t reg, uint8_t data)
{
    HAL_I2C_Mem_Write(&hi2c1, DevAddress, reg,I2C_MEMADD_SIZE_8BIT,&data,1,10);
}


//在AT24CXX里面的指定地址开始读出指定个数的数据
//ReadAddr :开始读出的地址 对24c02为0~255
//pBuffer  :数据数组首地址
//NumToRead:要读出数据的个数
void at24cxx_read(uint16_t DevAddress,uint8_t reg,uint8_t *pbuffer,uint16_t numtoread)
{
	HAL_I2C_Mem_Read(&hi2c1, DevAddress, reg,I2C_MEMADD_SIZE_8BIT,pbuffer,numtoread,10);
}
//在AT24CXX里面的指定地址开始写入指定个数的数据
//WriteAddr :开始写入的地址 对24c02为0~255
//pBuffer   :数据数组首地址
//NumToWrite:要写入数据的个数
void at24cxx_write(uint16_t DevAddress,uint8_t reg,uint8_t *pbuffer,uint16_t numtowrite)
{
	HAL_I2C_Mem_Write(&hi2c1, DevAddress, reg,I2C_MEMADD_SIZE_8BIT,pbuffer,numtowrite,10);
}



//检查AT24CXX是否正常
//这里用了24XX的最后一个地址(CHECK_ADDER)来存储标志字.
//如果用其他24C系列,这个地址要修改
//返回1:检测失败
//返回0:检测成功
uint8_t at24cxx_check(void)
{
	uint8_t temp;
	temp=at24cxx_read_single_reg(AT24C02_ADDER,CHECK_ADDER);//避免每次开机都写AT24CXX
	if(temp==0X55)return 0;
	else//排除第一次初始化的情况
	{
		at24cxx_write_single_reg(AT24C02_ADDER,CHECK_ADDER,0X55);
	    temp=at24cxx_read_single_reg(AT24C02_ADDER,CHECK_ADDER);
		if(temp==0X55)return 0;
	}
	return 1;
}

void at24cxx_rw_test(void)
{
	extern void log(char* fmt,...);
	extern void log_arr(uint8_t *arr,uint16_t len);
	uint8_t buff[10] = {0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b};
	log("\r\n[at24c02==>]write data:\r\n");
	log_arr(buff, 10);
	at24cxx_write(AT24C02_ADDER,0xf0,buff,10);
	memset(buff,0,10);
	osDelay(20);
	at24cxx_read(AT24C02_ADDER,0xf0,buff,10);
	log("\r\n[at24c02<==]read data:\r\n");
	log_arr(buff, 10);
	log("\r\n");
}
